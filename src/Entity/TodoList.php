<?php

namespace App\Entity;

use App\Repository\TodoListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TodoListRepository::class)]
class TodoList
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\ManyToOne(inversedBy: 'todoLists')]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $user = null;

    #[ORM\OneToMany(mappedBy: 'ToDoList', targetEntity: ToDoListPosition::class)]
    private Collection $toDoListPositions;

    public function __construct()
    {
        $this->toDoListPositions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection<int, ToDoListPosition>
     */
    public function getToDoListPositions(): Collection
    {
        return $this->toDoListPositions;
    }

    public function addToDoListPosition(ToDoListPosition $toDoListPosition): self
    {
        if (!$this->toDoListPositions->contains($toDoListPosition)) {
            $this->toDoListPositions->add($toDoListPosition);
            $toDoListPosition->setToDoList($this);
        }

        return $this;
    }

    public function removeToDoListPosition(ToDoListPosition $toDoListPosition): self
    {
        if ($this->toDoListPositions->removeElement($toDoListPosition)) {
            // set the owning side to null (unless already changed)
            if ($toDoListPosition->getToDoList() === $this) {
                $toDoListPosition->setToDoList(null);
            }
        }

        return $this;
    }
}
