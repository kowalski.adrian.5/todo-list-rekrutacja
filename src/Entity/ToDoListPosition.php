<?php

namespace App\Entity;

use App\Repository\ToDoListPositionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ToDoListPositionRepository::class)]
class ToDoListPosition
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?bool $done = null;

    #[ORM\ManyToOne(inversedBy: 'toDoListPositions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?ToDoList $ToDoList = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isDone(): ?bool
    {
        return $this->done;
    }

    public function setDone(bool $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getToDoList(): ?ToDoList
    {
        return $this->ToDoList;
    }

    public function setToDoList(?ToDoList $ToDoList): self
    {
        $this->ToDoList = $ToDoList;

        return $this;
    }
}
