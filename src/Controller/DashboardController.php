<?php

namespace App\Controller;

use App\Entity\TodoList;
use App\Entity\ToDoListPosition;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Form\Type\ToDoListType;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends AbstractController
{
    private $security;
    private $entityManager;
    
    public function __construct(Security $security, EntityManagerInterface $entityManager)
    {
       $this->security = $security;
       $this->entityManager = $entityManager;
    }

    #[Route('/dashboard', name: 'app_dashboard')]
    public function index(Request $request): Response
    {
        $toDoList = new TodoList();
        
        $form = $this->createForm(ToDoListType::class, $toDoList);
        $form->get('user')->setData($this->security->getUser());

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            
            $this->entityManager->persist($toDoList);
            $this->entityManager->flush();

        }

        $toDoLists = $this->entityManager->getRepository(TodoList::class)->findBy(['user' => $this->security->getUser()]);

        return $this->render('dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
            'form' => $form->createView(),
            'toDoLists' => $toDoLists
        ]);
    }

    
    #[Route('/add/position', name: 'app_add_position')]
    public function addPostion(Request $request): JsonResponse
    {
        $position = new ToDoListPosition();  
        
        $position->setName($request->get('name'));
        $position->setDone(false);
        $position->setToDoList($this->entityManager->getRepository(ToDoList::class)->find($request->get('id')));

        $this->entityManager->persist($position);
        $this->entityManager->flush();

        return new JsonResponse(['id' => $position->getId(), 'name' => $position->getName()]);
    }
    
        
    #[Route('/mark/position', name: 'app_mark_position')]
    public function markPostion(Request $request): JsonResponse
    {
        $position = $this->entityManager->getRepository(ToDoListPosition::class)->find($request->get('id'));  
    
        $position->setDone(filter_var($request->get('done'), FILTER_VALIDATE_BOOLEAN));

        $this->entityManager->persist($position);
        $this->entityManager->flush();

        return new JsonResponse([$position->isDone(), $request->get('done')]);
    }
}


