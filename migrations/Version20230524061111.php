<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230524061111 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE to_do_list_position (id INT AUTO_INCREMENT NOT NULL, to_do_list_id INT NOT NULL, name VARCHAR(255) NOT NULL, done TINYINT(1) NOT NULL, INDEX IDX_6BE36FDDB3AB48EB (to_do_list_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE to_do_list_position ADD CONSTRAINT FK_6BE36FDDB3AB48EB FOREIGN KEY (to_do_list_id) REFERENCES todo_list (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE to_do_list_position DROP FOREIGN KEY FK_6BE36FDDB3AB48EB');
        $this->addSql('DROP TABLE to_do_list_position');
    }
}
